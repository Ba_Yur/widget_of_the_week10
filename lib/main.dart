import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Sliver AppBar title'),
            floating: true,
            expandedHeight: 150,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              return Dismissible(
                background: ColoredBox(
                  color: Colors.red,
                ),
                key: UniqueKey(),
                child: SizedBox(
                  height: 50.0,
                  child: ListTile(
                    title: Center(
                      child: Text(
                        index.toString(),
                      ),
                    ),
                  ),
                ),
              );
            }, childCount: 30),
          ),
        ],
      ),
    );
  }
}
